VAULT_PASSWORD_FILE = --vault-password-file ~/.secrets/techops.vault
ANSIBLE_VAULT = $(shell which "ansible-vault" 2>/dev/null || echo "/usr/bin/ansible-vault" ) ${VAULT_PASSWORD_FILE}
ANSIBLE_PLAYBOOK = $(shell which "ansible-playbook" 2>/dev/null || echo "/usr/bin/ansible-playbook" )
ANSIBLE_CMD = ${ANSIBLE_PLAYBOOK} ${VAULT_PASSWORD_FILE}

# rewrite account
ifneq ($(strip $(aws_account)),)
  ANSIBLE_CMD=AWS_PROFILE=$(aws_account) ${ANSIBLE_PLAYBOOK} ${VAULT_PASSWORD_FILE}
  a=-e aws_account=$(aws_account)
else
  a=
endif

# rewrite env
ifneq ($(strip $(env)),)
  e=-e env=$(env)
else
  e=
endif

# rewrite region
ifneq (,$(findstring -, $(aws_region)))
  r=-e aws_region=$(aws_region)
else
  r=
endif

# rewrite commit
ifneq ($(strip $(commit)),)
  c=-e commit=$(commit)
else
  c=
endif

help:
## show this help
	@echo 'Available make targets:'
	@echo ''
	@grep -B1 '^## .*' ${MAKEFILE_LIST} | grep -v -- '^--$$' | sed -e 's/^## /	/' -e 's/^\([^:]*\):.*/  \1/'
	@echo ''

nagios-stack:
## deploy and configure nagios
	${ANSIBLE_CMD} nagios-stack.yml $(e) $(a) $(r) $(c) $(debug)

nagios-install:
## install nagios4 core application and dependencies
	${ANSIBLE_CMD} nagios-install.yml $(e) $(a) $(r) $(c) $(debug)

nagios-deploy:
## deploy and configure nagios application
	${ANSIBLE_CMD} nagios-deploy.yml $(e) $(a) $(r) $(c) $(debug)

nagios-client:
## deploy and configure nagios client instance
	${ANSIBLE_CMD} nagios-client.yml $(e) $(a) $(r) $(c) $(debug) -e app_name=$(app_name) -e app_role=$(app_role)

check:
## hack for check flag
	@echo check mode applied

edit-secrets:
## Edit staging secrets
	${ANSIBLE_VAULT} edit vars/secrets.yml


